var Anime = require('../models/anime'),
express = require('express'),
router = express.Router();

router.use(function(req, res, next) {  
    res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    next();
});

router.route('/animes').get(function (req, res) {

	var regex = new RegExp(req.query.search, 'i');
	Anime.find({title: regex}, function(err, animes) {
		if (err) {
			return res.send(err);
		} 
		res.json(animes);
	});
	
});

router.route('/animes/:id').get(function (req, res) {
	Anime.findOne({ _id: req.params.id}, function (err, anime) {
		if (err) {
			return res.send(err);
		}
		res.json(anime);
		console.log('sent');
	});
});

module.exports = router;

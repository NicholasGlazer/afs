var mongoose = require('mongoose');

var inc = require('mongoose-auto-increment');

var mongoURI = 'mongodb://'+process.env.MONGO_PORT_27017_TCP_ADDR+':'+process.env.MONGO_PORT_27017_TCP_PORT; //prod
// var mongoURI = 'mongodb://localhost:27017'; //dev

var MongoDB = mongoose.connect(mongoURI).connection;
inc.initialize(MongoDB);

MongoDB.on('error', function(err) {
	console.log(err.message);
});

MongoDB.once('open', function() {
	console.log('mongodb connection open');
});

module.exports = mongoose;
var mongoose = require('../connect'),
Schema = mongoose.Schema,
inc = require('mongoose-auto-increment');


var animeSchema = new Schema({
	title: String,
	type: String,
	rating: String,
	info: {
		poster: String,
		genre: Array,
		year: String,
		episodes: String,
		description : String,
		linkData: [{
			link: String,
			name: String
		}]
	},


});
animeSchema.plugin(inc.plugin, {
	model: 'Anime'
	// field: '_id'
});
module.exports = mongoose.model('Anime', animeSchema);

# Set the base image to Ubuntu
FROM node:latest

# File Author / Maintainer
MAINTAINER Nicholas Glazer <glazer.nicholas@gmail.com>

# Install Node.js and other dependencies
RUN apt-get update
RUN apt-get install -y python-software-properties software-properties-common curl openssh-server git supervisor 

RUN mkdir -p /var/run/sshd /var/log/supervisor /root/.ssh /home/node-server
#COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
#ADD authorized_keys /root/.ssh/authorized_keys

#ADD ./run.sh /run.sh
#RUN chmod 755 /run.sh

# Install npm dependencies
RUN npm install -g express bower cors


# Project
RUN git clone https://github.com/NicholasGlazer/afs.git /home/node-server/afs
# Install project dependencies
RUN cd /home/node-server/afs/ && npm install

# Git
#ADD post-receive /home/node-server/afs/.git/hooks/post-receive
#RUN chmod +x /home/node-server/afs/.git/hooks/post-receive
#RUN git --git-dir='/home/node-server/afs/.git' config receive.denyCurrentBranch ignore


# Bundle app source 
COPY . /home/node-server/afs

# Expose port
EXPOSE  8001

CMD ["/bin/bash", "/home/node-server/afs/run.sh"]
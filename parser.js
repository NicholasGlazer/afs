var x = require('x-ray')(),
Anime = require('./models/anime'),
async = require('async');

var urlScrap = [
	{
		url: 'http://yummyanime.com/catalog',
		scope: '.anime-column', 
		selectors: [
			 	{
					title: '.anime-title',
					type: '.anime-type',
					rating: '.rating-info',
				    info: x('a@href', {
				    	engTitle: '.altname-list li',
						poster: '.content-img-block img@src',
					  	genre: x('.categories-list', ['a']),
					  	year: 'ul.content-main-info>li:nth-of-type(3)',
					  	// producer: 'ul.content-main-info>li:nth-of-type(4)',
					  	episodes: 'ul.content-main-info>li:nth-of-type(5)',
					  	// translate: 'ul.content-main-info>li:nth-of-type(6)',
					  	// sound: 'ul.content-main-info>li:nth-of-type(7)',
						description: '.content-desc',
						linkData: x('.video-button', [{
							name: 'a',
							data: '@data-id',
							link: '@href'
						}])
					})
				}
			]
	}
];

function saveMongo(err, items) {
	if (err) {
		console.log('Error: ' + err);
	};
	async.eachSeries(items, function(item, callback) {
		var dbItem = new Anime(item);

		dbItem.save(function(err, data) {
			if(err) {
				console.log('Anime was Not written to DB: ' + err);
			}
			else {
				console.log('Anime was written to DB: ' + data);
				callback()
			};
		});
	});

};

x(urlScrap[0].url, urlScrap[0].scope, urlScrap[0].selectors)(saveMongo)
	.paginate('.pagination li a@href')
	.limit(2)
	;